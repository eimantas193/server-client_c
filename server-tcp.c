#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define SERVER_PORT 5000

int main (void)
{
	int listen_sd, accept_sd, on = 1, d, i;
	char buffer[80];
	struct sockaddr_in addr;

	listen_sd = socket(AF_INET, SOCK_STREAM, 0);

        setsockopt(listen_sd,
	SOL_SOCKET, SO_REUSEADDR,
	(char *)&on, sizeof(on));

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr.sin_port = htons(SERVER_PORT);
	bind(listen_sd, ( struct sockaddr *)&addr, sizeof(addr));

	listen(listen_sd, 5);
	printf("Server is ready \n");

	accept_sd = accept(listen_sd, NULL, NULL);
	printf("Client is connected\n");

        printf("Sending FTPD\n");
        send(accept_sd, "Hello\r\n", 11, 0);

        d=recv(accept_sd, buffer, sizeof(buffer),0);
        printf("Received USER \n");
        printf("%s", buffer);

	printf("Sending FTPD\n");
        send(accept_sd, "Please specify the passsword.\n",34, 0);
        

	printf("Server is waiting a mesage\n");
	d = recv(accept_sd, buffer, sizeof(buffer), 0);
	printf("Received message \n");
	printf("%s", buffer);

	printf("Sending message back\n");
	send(accept_sd, buffer, d, 0);

	printf("Close connection\n");
	close(accept_sd);

	printf("Close socket\n");
	close(listen_sd);


	return(0);
}
